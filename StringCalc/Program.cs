using System;
using NUnit.Framework;


namespace StringCalc
{
    [TestFixture]
    public class StringCalculator
    {
        [TestCase(0, "")]
        [TestCase(1, 1)]
        [TestCase(3, "1,2")]
        [TestCase(6, "1,2,3")]
        [TestCase(6, "1\n2,3")]
        [TestCase(3, "//;\n1;2")]
        public void AddNumberReturntSum(int expected, string number)
        {
            Assert.AreEqual(expected, Calculator.Add(number));
        }

        [Test]
        public void AddNegativeNumberThrowException()
        {

            const string number = "1\n-2,-3";
            var exception = Assert.Throws<Exception>(() => Calculator.Add(number));
            Assert.AreEqual("negatives not allowed -2 -3", exception.Message);

        }
    }


}
